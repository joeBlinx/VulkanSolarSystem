//
// Created by stiven on 17-10-08.
//

#ifndef GLENGINE_MATERIALMANAGER_H
#define GLENGINE_MATERIALMANAGER_H


#include <utility>
#include <vector>

#include <string>
#include <glEngine/parser/parser.h>
#include <glEngine/utils/utils.h>
#include <glEngine/manager/assetsList.h>

namespace glEngine {

    class MaterialManager {

        std::string path{"assest/material"};
        AssetsList<mapMaterials > materials;
        static MaterialManager materialManager;
        MaterialManager() = default;
    public :
        static int get(std::string && pathMaterial);
        static mapMaterials get(int i );


    };


}
#endif //GLENGINE_MATERIALMANAGER_H
