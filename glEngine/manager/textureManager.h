//
// Created by stiven on 17-10-20.
//

#ifndef GLENGINE_TEXTUREMANAGER_H
#define GLENGINE_TEXTUREMANAGER_H

#include <glish/Texture.hpp>
#include "assetsList.h"

namespace glEngine {
    class TextureManager {

        static TextureManager textureManager;
        AssetsList<glish::Texture> textures;
        TextureManager();

    public:
        static int get(std::string && path);
        glish::Texture const & get(int i)const;
        static void use(int i, int numberTexture);

    };
}

#endif //GLENGINE_TEXTUREMANAGER_H
