#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0)out vec3 norm;
layout(location = 1)out vec2 uvOut;
layout(location = 2) out vec3 pos;
layout(location = 3) out int instance_id;
layout(location = 4) out vec3 posViewSpace;
layout(location = 5)out int whichObject;
layout(location = 6)out vec3 light;
//
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normals;

layout(set = 0, binding = 0) uniform transform{
                            	mat4 model;
                            	mat4 proj;
                            	mat4 view;

}view[3];

layout(push_constant) uniform MyConstants {
    int something;
}constt;

layout (set = 0, binding = 4) uniform a{
    int ob;
}object;


// l = light
//n = normal plan
// d = delta
mat4 matProjShadow(vec3 l, vec3 n, float d){
    return mat4(dot(n, l) + d - l.x*n.x, -l.y*n.x, -l.z*n.x, -n.x,
                -l.x*n.y, dot(n,l) + d -l.y*n.y, -l.z*n.y, -n.y,
                -l.x*n.z, -l.y*n.z, dot(n,l) + d + -l.z*n.z, -n.z,
                -l.x*d, -l.y*d, -l.z*d, dot(n,l));
}


void main() {
    vec3 posLight = vec3(0, -200, 0);
    light = posLight;
    mat4 model = view[gl_InstanceIndex].model;
    if(constt.something == 0){
        mat4 mult = view[gl_InstanceIndex].proj * view[gl_InstanceIndex].view * view[gl_InstanceIndex].model;
        gl_Position = mult*vec4(inPosition, 1.0);
        pos = vec3(model *vec4(inPosition, 1));
           uvOut = uv;

    }else if(constt.something == 1){
        const float size = 500;
        mat3 model = mat3(size,0,0,
        0,0,0,
        0,0,size

        );
        vec3 buffere = model*inPosition;
        buffere.y += 50;
        gl_Position = view[0].proj*view[0].view*vec4(buffere, 1);
        pos = buffere;
        uvOut  =  uv;

    }else if(constt.something == 3){
    // mirror
            model[3][1] += inPosition.y + 100;
    //
            mat4 views = view[gl_InstanceIndex].view ;
           // views[1][1] *=-1;
            mat4 mult = view[gl_InstanceIndex].proj * views * model;

             gl_Position = mult*vec4(inPosition, 1.0);
             pos = vec3(model *vec4(inPosition, 1));
             uvOut = uv;
             uvOut.y = 1- uvOut.y;
    }
    else{
        float plan = -50;
        // mirror
        //model[3][1] += 100;
//
////        views[1][1] *=-1;
//         gl_Position = mult*vec4(inPosition, 1.0);

    //try projection shadow
        mat4 projMatrice = mat4(posLight.y , 0, 0, 0,
                                -posLight.x, 0, -posLight.z, -1,
                                 0, 0, posLight.y , 0,
                                0, 0, 0, posLight.y);

        mat4 projTest = matProjShadow(posLight, vec3(0, 1, 0), -50);
         mat4 mult = view[gl_InstanceIndex].proj * view[gl_InstanceIndex].view * model;

        vec4 tset  = projTest* model*vec4(inPosition, 1);
        tset /= tset.w;
        vec3 temp = vec3(tset);
//        tset.y = 0;
//        tset.z = (posLight.y*temp.z - posLight.z*temp.y)/(posLight.y - temp.y) ;
//        tset.x = (posLight.y*temp.x - posLight.x*temp.y)/(posLight.y - temp.y) ;
        gl_Position = view[gl_InstanceIndex].proj * view[gl_InstanceIndex].view *tset;
//        gl_Position.y = 0;
         pos = vec3(model *vec4(inPosition, 1));
            uvOut = uv;
    }

    norm = mat3(model)*normals;

    instance_id = gl_InstanceIndex;


    posViewSpace = gl_Position.xyz;
    whichObject = constt.something;
}