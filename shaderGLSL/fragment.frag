#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0)in vec3 norm;
layout(location = 1)in vec2 uv;
layout (location = 2) in vec3 pos;
layout(location = 3) in flat int instance_id;
layout(location = 4) in vec3 posViewSpace;
layout(location = 5) in flat int object;
layout(location = 6) in vec3 posLight;
const int numbrePlanet = 3;




layout(set = 0, binding = 1) uniform light{
    vec3 pos;
    float size;
}Light[numbrePlanet];

layout(set = 0, binding = 2) uniform sampler2D image[numbrePlanet];
layout (set = 0, binding = 3) uniform samplerCube cubeMap;
// just polynomial equation ax2 + bx + c = 0
bool rayCast( vec3 center, float radius, vec3 ray){


    float b = dot(ray ,-center);
    float c = dot(center, center) - radius*radius;

    return (b*b -c)>= 0;

}

vec4 planet(){


    float distanceToSun = length(Light[instance_id].pos);
    vec4 color = texture(image[instance_id], uv);
//    outColor += texture(image[instance_id], uv);
    vec3 light1 = -normalize(pos);
   if(instance_id == 4){
            vec3 dirView = posViewSpace;
            vec3 ref = refract(dirView, light1, 1/1.52);
            ref.y = -ref.y;
            color = texture(cubeMap, ref) ;
    }

    return color;
}


vec3 dirLight = normalize(posLight - pos);
float power = 1;

float calculateLight(vec3 normal, int id){

    vec3 light1 = -normalize(pos);
    float diffuseLight = 0;
    float delta = max(dot(light1, normalize(normal)), 0);

    float coef = 1;
    if(id > 0){
        coef = 0.2+delta;
    }

    coef += power*max(dot(dirLight, normalize(normal)), 0);
     if(id > 0){
         for(int i = 1 ; i< id ; i++){
             bool collide = rayCast(Light[i].pos, Light[i].size, light1);
              if(collide && length(pos) < length(Light[instance_id].pos)){
                 coef -= 0.5;
                 break;
               }
            }
        }

    return coef;
}
void main() {
    float delta;
  if(object == 0 || object == 3){
    delta = calculateLight(norm, instance_id);
    outColor = planet();
  }else if (object == 2){

    outColor = vec4(0, 0, 0, 0);

  }else{
    vec3 normal = normalize(vec3(0, -1, 0));
  //  delta = power*max(dot(dirLight, normalize(vec3(0, -1, 0))), 0);
    delta = calculateLight(normal, 4);
    outColor = vec4(0.5, 0.5, 0.5, 1);
      for(int i = 0 ; i< 4 ; i++){
            bool collide = rayCast(Light[i].pos, Light[i].size, dirLight);
              if(collide ){
                 delta -= 0.5;
                 break;
            }
       }
    //outColor.xyz *= (delta+ 0.2);

  }
    outColor.xyz *= delta;

    // try fog
    float coefFog = (1000 - posViewSpace.z)/(1000-50);
    vec4 colorFog = vec4(0.7, 0.7, 0.7, 1);
    outColor = coefFog*outColor + (1-coefFog)*colorFog;


}
