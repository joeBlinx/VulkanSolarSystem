//
// Created by stiven on 17-12-01.
//

#ifndef SOLARSYSTEM_TRANSFOM_HPP
#define SOLARSYSTEM_TRANSFOM_HPP

#include <glm/glm.hpp>
struct transform{
	glm::mat4 model;
	glm::mat4 proj;
	glm::mat4 view;

};
#endif //SOLARSYSTEM_TRANSFOM_HPP
