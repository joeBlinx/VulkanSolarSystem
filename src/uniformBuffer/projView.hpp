//
// Created by stiven on 17-12-06.
//

#ifndef SOLARSYSTEM_PROJVIEW_HPP
#define SOLARSYSTEM_PROJVIEW_HPP

#include <glm/glm.hpp>

struct projView{
	glm::mat4 proj;
	glm::mat4 view;
};
#endif //SOLARSYSTEM_PROJVIEW_HPP
