//
// Created by stiven on 17-12-01.
//

#include <utils/buffer.hpp>
#include <cstring>
#include "uniformBuffer.hpp"
#include "transfom.hpp"
#include <glm/gtx/transform.hpp>
#include <scene.hpp>
#include "position.hpp"
#include "projView.hpp"

UniformBuffer::UniformBuffer(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice)
		: logicalDevice(logicalDevice),
		  chooseObject(logicalDevice, physicalDevice) {


	VkDeviceSize size = sizeof(transform);
	VkDeviceSize sizePos = sizeof(pos);
	for(unsigned i = 0; i< uni.size(); i++) {
		createBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uni[i], memory[i],
		             logicalDevice, physicalDevice);
		createBuffer(sizePos, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, posUni[i], posMemory[i],
		             logicalDevice, physicalDevice);
	}
	VkDeviceSize sizeCam = sizeof(projView);
	createBuffer(sizeCam, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniCam, uniCamMemory,
		             logicalDevice, physicalDevice);

	chooseObject.update(object{1});

}

UniformBuffer::~UniformBuffer() {

	for (unsigned i = 0; i < Scene::numberPlanet ; i++){
		vkDestroyBuffer(logicalDevice, uni[i], nullptr);
		vkFreeMemory(logicalDevice, memory[i], nullptr);
		vkDestroyBuffer(logicalDevice, posUni[i], nullptr);
		vkFreeMemory(logicalDevice, posMemory[i], nullptr);

	}
	vkDestroyBuffer(logicalDevice, uniCam, nullptr);
	vkFreeMemory(logicalDevice, uniCamMemory, nullptr);

}


void UniformBuffer::update(int size, const void *datas, VkDeviceMemory memory) {
	void *data;
	vkMapMemory(logicalDevice, memory, 0, size, 0, &data);
	memcpy(data, datas, size);
	vkUnmapMemory(logicalDevice, memory);
}

void UniformBuffer::update(const uniformArray &newTransform, posArray const &newPos) {

	VkDeviceSize size = sizeof(transform);
	for(unsigned i = 0; i< uni.size(); i++) {
		update(size, &newTransform[i], memory[i]);
		update(sizeof(pos), &newPos [i], posMemory[i]);
	}

	projView p{newTransform[0].proj, newTransform[0].view};
	update(sizeof(projView), &p, uniCamMemory);
}

VkBuffer UniformBuffer::getPosUni(int i) {
	return posUni[i];
}

VkBuffer UniformBuffer::getUniCam() const {
	return uniCam;
}

Uniform<object> &UniformBuffer::getObject() {
	return chooseObject;
}
