//ok y'avais une signification, mais ca d
// Created by stiven on 17-12-01.
//

#ifndef SOLARSYSTEM_UNIFORMBUFFER_HPP
#define SOLARSYSTEM_UNIFORMBUFFER_HPP


#include <vulkan/vulkan.h>
#include <initialisation/logicaldevice.hpp>
#include <scene.hpp>
#include "transfom.hpp"
#include "chooseObject.hpp"
#include "uniform.hpp"

class UniformBuffer {

	std::array<VkBuffer, Scene::numberPlanet> uni;
	std::array<VkDeviceMemory, Scene::numberPlanet> memory ;
	LogicalDevice logicalDevice;

	std::array<VkBuffer, Scene::numberPlanet> posUni;
	std::array<VkDeviceMemory, Scene::numberPlanet> posMemory ;

	VkBuffer uniCam;
	VkDeviceMemory uniCamMemory;

	Uniform<object> chooseObject;



	void update(int size, const void *datas, VkDeviceMemory memory);


public:

	UniformBuffer(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice);
	void update(const uniformArray &newTransform, posArray const &newPos);
	VkBuffer operator[](int i){ return uni[i];}
	VkBuffer getPosUni(int i);

	VkBuffer getUniCam() const;

	Uniform<object> & getObject();

	virtual ~UniformBuffer();
};


#endif //SOLARSYSTEM_UNIFORMBUFFER_HPP
