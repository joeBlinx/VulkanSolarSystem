//
// Created by stiven on 17-12-04.
//

#include "PlanetsTexture.hpp"
PlanetsTexture::PlanetsTexture(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &commandPool)
		: logicalDevice(logicalDevice), physicalDevice(physicalDevice), commandPool(commandPool) {
	std::array<std::string, Scene::numberPlanet> paths{"sol/sun.jpg", "earth/earth.jpg", "mars/mars.jpg", "Moon.jpg", "Moon.jpg"};

	for(auto &a : paths){
		init(a);
	}
}




void PlanetsTexture::init(std::string const &path) {

	textures[number++] = Textures(logicalDevice, commandPool, "3DModels/"+path, physicalDevice);
}
