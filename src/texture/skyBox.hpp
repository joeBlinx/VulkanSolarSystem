//
// Created by stiven on 17-12-08.
//

#ifndef SOLARSYSTEM_SKYBOX_HPP
#define SOLARSYSTEM_SKYBOX_HPP


#include <vulkan/vulkan.h>
#include <initialisation/commandPool.hpp>
#include <initialisation/logicaldevice.hpp>


class SkyBox {

	const std::array<std::string, 6> ext{"rt", "lf", "up", "dn", "bk", "ft"};
	LogicalDevice * device = nullptr;
	CommandPool * commandPool = nullptr;
	std::string path;

	PhysicalDevice & physicalDevice;
	VkImage image;
	VkImageView imageView;
	VkSampler sampler;
	VkDeviceMemory memory;

public:

	SkyBox(LogicalDevice *device, CommandPool *commandPool, const std::string &path, PhysicalDevice &physicalDevice);

	virtual ~SkyBox();

	VkImageView getImageView() const;

	VkSampler getSampler() const;

	void createImageSkyBox(int texWidth, int texHeight);
};


#endif //SOLARSYSTEM_SKYBOX_HPP
