//
// Created by stiven on 17-12-04.
//

#ifndef SOLARSYSTEM_PLANETSTEXTURE_HPP
#define SOLARSYSTEM_PLANETSTEXTURE_HPP


#include "textures.hpp"
#include "../scene.hpp"

class PlanetsTexture {
	LogicalDevice & logicalDevice;
	PhysicalDevice & physicalDevice;
	CommandPool & commandPool;
public:
	PlanetsTexture(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &commandPool);

private:

	std::array<Textures, Scene::numberPlanet> textures;
	int number = 0;
public:
	void init(std::string const &  path);
	Textures & operator[](int i ){ return textures[i];}
};


#endif //SOLARSYSTEM_PLANETSTEXTURE_HPP
