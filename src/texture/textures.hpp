//
// Created by stiven on 17-12-04.
//

#ifndef SOLARSYSTEM_TEXTURES_HPP
#define SOLARSYSTEM_TEXTURES_HPP


#include <initialisation/logicaldevice.hpp>
#include <initialisation/commandPool.hpp>

class Textures {
	LogicalDevice * device = nullptr;
	CommandPool * commandPool = nullptr;
	std::string name ;

	VkImage image;
	VkDeviceMemory imageMemory;
	VkImageView imageView;

	VkSampler sampler;

public:
	Textures() = default;

	Textures(LogicalDevice &device, CommandPool &commandPool, const std::string &name,
		         PhysicalDevice &physicalDevice);

	Textures & operator=(Textures const&) = delete;
	Textures (Textures const&) = delete;
	Textures & operator=(Textures && tex) ;
	Textures (Textures && tex) ;
	operator VkImageView &(){ return imageView;}

	VkSampler getSampler() ;

	virtual ~Textures();
};


#endif //SOLARSYSTEM_TEXTURES_HPP
