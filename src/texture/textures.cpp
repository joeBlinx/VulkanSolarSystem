//
// Created by stiven on 17-12-04.
//

#include "textures.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include <lib/stb_image.h>
#include <utils/buffer.hpp>
#include <utils/image.hpp>

Textures::Textures(LogicalDevice &device, CommandPool &commandPool, const std::string &name,
                   PhysicalDevice &physicalDevice) : device(&device),
                                                     commandPool(&commandPool),
                                                     name(name) {
	int texWidth, texHeight, texChannels;
	stbi_uc *pixels = stbi_load(name.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
	VkDeviceSize imageSize = texWidth * texHeight * 4;

	if (!pixels) {
		throw std::runtime_error("failed to load texture image!");
	}
	VkBuffer buffer;
	VkDeviceMemory memory;
	createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
	             VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
	             buffer, memory, device, physicalDevice);

	void *data;
	vkMapMemory(device, memory, 0, imageSize, 0, &data);
	memcpy(data, pixels, imageSize);
	vkUnmapMemory(device, memory);

	createImage(texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
	            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
	            image, imageMemory, device, physicalDevice);

	transitionImageLayout(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED,
	                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
	                      commandPool, device, 1);
	copyBufferToImage(commandPool, device, buffer, image, static_cast<uint32_t>(texWidth),
	                  static_cast<uint32_t>(texHeight), 0);
	transitionImageLayout(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
	                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	                      commandPool, device, 1);

	vkDestroyBuffer(device, buffer, nullptr);
	vkFreeMemory(device, memory, nullptr);

	imageView = createImageView(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, device, 1);


	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;
	if (vkCreateSampler(device, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler!");
	}
}

Textures::~Textures() {
	vkDestroyImage(*device, image, nullptr);
	vkFreeMemory(*device, imageMemory, nullptr);
	vkDestroyImageView(*device, imageView, nullptr);
	vkDestroySampler(*device, sampler, nullptr);

}

Textures &Textures::operator=(Textures &&tex) {
	device = tex.device;
	commandPool = tex.commandPool;
	name = tex.name ;
	image = std::move(tex.image);
	imageMemory = tex.imageMemory;
	imageView = tex.imageView;
	sampler = tex.sampler;
	tex.image = 0;
	tex.imageMemory = 0;
	tex.imageView = 0;
	tex.sampler = 0;
	return *this;
}

Textures::Textures(Textures &&tex):device(tex.device), commandPool(tex.commandPool),
name(std::move(tex.name)), image(tex.image), imageMemory(tex.imageMemory), imageView(tex.imageView),
sampler(tex.sampler){

	tex.image = 0;
	tex.imageMemory = 0;
	tex.imageView = 0;
	tex.sampler = 0;
}

 VkSampler Textures::getSampler()  {
	return sampler;
}
