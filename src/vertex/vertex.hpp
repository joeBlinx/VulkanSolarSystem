//
// Created by stiven on 17-11-22.
//

#ifndef SOLARSYSTEM_VERTEX_HPP
#define SOLARSYSTEM_VERTEX_HPP


#include <vulkan/vulkan.h>
#include <initialisation/logicaldevice.hpp>
#include <initialisation/commandPool.hpp>

class Vertex {

	LogicalDevice & logicalDevice;
	PhysicalDevice &physicalDevice;
	CommandPool &commandPool;
	VkBuffer planetBuffer;
	VkDeviceMemory planetMemory;


	VkBuffer box;
	VkDeviceMemory boxMemory;

	VkBuffer skyBoxbuffer;
	VkDeviceMemory skyBoxMemory;

	unsigned size;

	void createVertexBuffer(VkBuffer &buffer, VkDeviceMemory &memory, std::string &&path);
public:
	Vertex(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &pool);

	operator VkBuffer&(){ return planetBuffer;}

	VkBuffer getSkyBoxBuffer(){return skyBoxbuffer;}
	VkBuffer getBoxBuffer(){ return box;}

	unsigned int getSize() const;

	~Vertex();

};


#endif //SOLARSYSTEM_VERTEX_HPP
