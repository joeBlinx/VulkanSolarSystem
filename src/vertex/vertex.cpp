//
// Created by stiven on 17-11-22.
//

#include "vertex.hpp"
#include "vertexModel.hpp"
#include "skyboxVertices.h"
#include <glEngine/parser/parser.h>
#include <utils/buffer.hpp>
#include <cstring>

Vertex::Vertex(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &pool) : logicalDevice(logicalDevice), physicalDevice(physicalDevice), commandPool(pool) {

	createVertexBuffer(box, boxMemory, "Crate/Crate1.obj");
	createVertexBuffer(planetBuffer, planetMemory, "3DModels/earth/earth.obj");

	unsigned size = 36*sizeof(skyboxVertices);
	VkBuffer staging;
	VkDeviceMemory stagingMemory;
	createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
	             staging, stagingMemory, logicalDevice, physicalDevice);

	createBuffer(size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, skyBoxbuffer, skyBoxMemory, logicalDevice, physicalDevice);


	void * datas;
	vkMapMemory(logicalDevice, stagingMemory, 0, size, 0, &datas);
	memcpy(datas, skyboxVertices, sizeof(skyboxVertices));

	vkUnmapMemory(logicalDevice, stagingMemory);

	copyBuffer(staging, skyBoxbuffer, sizeof(skyboxVertices), logicalDevice, pool);



	vkDestroyBuffer(logicalDevice, staging, nullptr);
	vkFreeMemory(logicalDevice, stagingMemory, nullptr);



}

void Vertex::createVertexBuffer(VkBuffer &buffer, VkDeviceMemory &memory, std::string &&path) {

	glEngine::parsing_data data = glEngine::parseObj(path);
	std::vector<vertexModel> models;
	models.reserve(data.vertices.size()/3);

	for(unsigned i = 0; i< data.vertices.size(); i++){
		models.emplace_back();
		models[i].pos = glm::normalize(data.vertices[i]);
		models[i].uv = data.uvs[i];
		models[i].normal = data.normals[i];
	}

	this->size = data.vertices.size();

	unsigned size = models.size()*sizeof(vertexModel);
	VkBuffer staging;
	VkDeviceMemory stagingMemory;

	createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
	             staging, stagingMemory, logicalDevice, physicalDevice);

	void * datas;
	vkMapMemory(logicalDevice, stagingMemory, 0, size, 0, &datas);
	memcpy(datas, models.data(), size);

	vkUnmapMemory(logicalDevice, stagingMemory);

	createBuffer(size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer, memory, logicalDevice, physicalDevice);

	copyBuffer(staging, buffer, size, logicalDevice, commandPool);

	vkDestroyBuffer(logicalDevice, staging, nullptr);
	vkFreeMemory(logicalDevice, stagingMemory, nullptr);
}



Vertex::~Vertex() {
	vkDestroyBuffer(logicalDevice, planetBuffer, nullptr);
	vkFreeMemory(logicalDevice, planetMemory, nullptr);

	vkDestroyBuffer(logicalDevice, skyBoxbuffer, nullptr);
	vkFreeMemory(logicalDevice, skyBoxMemory, nullptr);
	vkDestroyBuffer(logicalDevice, box, nullptr);
	vkFreeMemory(logicalDevice, boxMemory, nullptr);

}

unsigned int Vertex::getSize() const {
	return size;
}
