//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_VERTEXMODEL_HPP
#define SOLARSYSTEM_VERTEXMODEL_HPP


#include <vulkan/vulkan.h>
#include <array>
#include <glm/glm.hpp>
struct vertexModel {
	static constexpr unsigned numberData = 3;
	glm::vec3 pos;
	glm::vec2 uv;
	glm::vec3 normal;

	static VkVertexInputBindingDescription getBindingDescription();
	static std::array <VkVertexInputAttributeDescription, numberData> getAttributeDescription();
};

struct vertexSkyBox{
	static constexpr unsigned numberData = 1;
	glm::vec3 pos;
	static VkVertexInputBindingDescription getBindingDescription();
	static VkVertexInputAttributeDescription getAttributeDescription();
};


#endif //SOLARSYSTEM_VERTEXMODEL_HPP
