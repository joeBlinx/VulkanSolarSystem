//
// Created by stiven on 17-11-21.
//

#include "vertexModel.hpp"

VkVertexInputBindingDescription vertexModel::getBindingDescription() {
	VkVertexInputBindingDescription input;
	input.binding = 0;
	input.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	input.stride = sizeof(vertexModel);
	return input;
}

std::array<VkVertexInputAttributeDescription, vertexModel::numberData> vertexModel::getAttributeDescription() {
	std::array<VkVertexInputAttributeDescription, numberData> input;

	input[0].binding = 0;
	input[0].location = 0;
	input[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	input[0].offset = offsetof(vertexModel, pos);

	input[1].binding = 0;
	input[1].location = 1;
	input[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	input[1].offset = offsetof(vertexModel, uv);

	input[2].binding = 0;
	input[2].location = 2;
	input[2].format = VK_FORMAT_R32G32B32_SFLOAT;
	input[2].offset = offsetof(vertexModel, normal);

	return input;
}

VkVertexInputBindingDescription vertexSkyBox::getBindingDescription() {
	VkVertexInputBindingDescription input;
	input.binding = 0;
	input.stride = sizeof(vertexSkyBox);
	input.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	return input;
}

VkVertexInputAttributeDescription vertexSkyBox::getAttributeDescription() {
	VkVertexInputAttributeDescription input;
	input.binding = 0;
	input.location = 0;
	input.format = VK_FORMAT_R32G32B32_SFLOAT;
	input.offset = offsetof(vertexSkyBox, pos);
	return input;
}
