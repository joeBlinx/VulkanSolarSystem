//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_LOOP_HPP
#define SOLARSYSTEM_LOOP_HPP


#include <initialisation/window.hpp>
#include <initialisation/surface.hpp>
#include <initialisation/initAll.hpp>
#include "scene.hpp"

class Loop {

	initAll init;
	VkSemaphore imageAvailableSemaphore;
	VkSemaphore renderFinishedSemaphore;
	Scene  & scene;
	bool pressed = false;
	static constexpr float angle = 0.05f;
	static constexpr float move = 5.0f;
	void draw();
	void createSemaphore();
public:
	Loop(Scene &scene);
	void loop();
	void moveCamera(int key);
	void mouseClick(GLFWwindow *wndow, int button, int action, int mods);
	void mouseMove(GLFWwindow *window, double x, double y);
	void zoom(GLFWwindow* window, double xoffset, double yoffset);
	~Loop();
};


#endif //SOLARSYSTEM_LOOP_HPP
