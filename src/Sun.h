//
// Created by lysilia on 19/11/17.
//

#ifndef VALKYRIA_SUN_H
#define VALKYRIA_SUN_H


#include <glm/glm.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <string>

class Sun {

protected:
    std::string name;
    std::string model;
    float size;
    glm::vec3 pos;

    glm::vec3 updatePosition;

public:

    Sun(const std::string &name, const std::string &model, float size, const glm::vec3 &pos);

    virtual glm::mat4 update(float deltaTime);

    const std::string &getModel() const;
    const std::string &getName() const;
    const glm::vec3 &getPos() const;
    const glm::vec3 &getUpdatePosition() const;

	float getSize() const;

};


#endif //VALKYRIA_SUN_H
