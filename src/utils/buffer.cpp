//
// Created by stiven on 17-11-22.
//

#include "buffer.hpp"

void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer &buffer,
                  VkDeviceMemory &bufferMemory, LogicalDevice &device, PhysicalDevice &physicalDevice) {


	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;


	if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
		throw std::runtime_error("failed to create planetBuffer!");
	}

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties, physicalDevice);

	if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate planetBuffer planetMemory!");
	}

	vkBindBufferMemory(device, buffer, bufferMemory, 0);
}

void copyBuffer(VkBuffer src, VkBuffer dest, VkDeviceSize size, LogicalDevice & logicalDevice, CommandPool & pool){
	VkCommandBuffer commandBuffer = beginSingleTimeCommands(logicalDevice, pool);

	VkBufferCopy copy;
	copy.size = size;
	copy.dstOffset = 0;
	copy.srcOffset = 0;

	vkCmdCopyBuffer(commandBuffer, src, dest, 1, &copy);
	endSingleTimeCommands(commandBuffer, logicalDevice, pool);
	
}