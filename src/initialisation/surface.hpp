//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_SURFACE_HPP
#define SOLARSYSTEM_SURFACE_HPP


#include <vulkan/vulkan.h>
#include "instance.hpp"
#include "window.hpp"

class Surface {

	VkSurfaceKHR surface;
	Instance & instance;
public:

	Surface(Instance &instance, Window &window);
	operator VkSurfaceKHR (){ return surface;}
	~Surface();
};


#endif //SOLARSYSTEM_SURFACE_HPP
