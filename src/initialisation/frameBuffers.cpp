//
// Created by stiven on 17-11-21.
//

#include "frameBuffers.hpp"
#include "depthImage.hpp"


FrameBuffers::~FrameBuffers() {

	for(auto & frameBuffer : swapChainFrambuffer){
		vkDestroyFramebuffer(logicalDevice, frameBuffer, nullptr);
	}

}

FrameBuffers::FrameBuffers(LogicalDevice &logicalDevice, SwapChain &swapChain, RenderPasses &renderPasses,
                           DepthImage &depthImage)
		: logicalDevice(logicalDevice), swapChain(swapChain), renderPasses(renderPasses),
depthImage(depthImage){

	swapChainFrambuffer.resize(swapChain.getSwapChainImageViews().size());

	for(unsigned i = 0; i < swapChainFrambuffer.size() ; i++){
		VkImageView attachment[] = {
				swapChain.getSwapChainImageViews()[i],
				depthImage
		};
		VkFramebufferCreateInfo framebufferInfo {};

		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPasses;
		framebufferInfo.attachmentCount = 2;
		framebufferInfo.pAttachments = attachment;
		framebufferInfo.width = swapChain.getSwapChainExtent().width;
		framebufferInfo.height = swapChain.getSwapChainExtent().height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(logicalDevice, &framebufferInfo, nullptr, &swapChainFrambuffer[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create framebuffer!");
		}

	}
}


