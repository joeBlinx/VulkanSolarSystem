//
// Created by stiven on 17-11-20.
//

#include <scene.hpp>
#include "initAll.hpp"

initAll::initAll() : instance(),
                     surface(instance, window),
                     physicalDevice(instance, surface),
                     logicalDevice(physicalDevice),
                     swapChain(logicalDevice, physicalDevice, surface),
                     renderPasses(swapChain, logicalDevice, physicalDevice),
                     descriptorSetLayout(logicalDevice),
                     graphicsPipeline(logicalDevice, swapChain, renderPasses, descriptorSetLayout),
                     commandPool(physicalDevice, logicalDevice),
                     depthImage(logicalDevice, physicalDevice, commandPool, swapChain),
                     frameBuffers(logicalDevice, swapChain, renderPasses, depthImage),
                     vertex(logicalDevice, physicalDevice, commandPool),
                     uniformBuffer(logicalDevice, physicalDevice),
		             planetsTexture(logicalDevice, physicalDevice, commandPool),
                     skyBox(&logicalDevice, &commandPool,"ame_nebula/violentdays_", physicalDevice),
                     descriptorPool(logicalDevice),
                     descriptorSet(logicalDevice, descriptorPool, descriptorSetLayout, uniformBuffer, planetsTexture,
                                   skyBox,
                                   uniformBuffer.getObject()),
                     commandBuffers(swapChain, commandPool, logicalDevice, renderPasses, frameBuffers, graphicsPipeline,
                                    vertex, descriptorSet, uniformBuffer) {

}

Window & initAll::getWindow()  {
	return window;
}

LogicalDevice & initAll::getLogicalDevice()  {
	return logicalDevice;
}

SwapChain & initAll::getSwapChain()  {
	return swapChain;
}

const CommandBuffers &initAll::getCommandBuffers() const {
	return commandBuffers;
}

const GraphicsPipeline &initAll::getGraphicsPipeline() const {
	return graphicsPipeline;
}

void initAll::update(const uniformArray &t, posArray const &pos) {
	uniformBuffer.update(t, pos);

}

initAll::~initAll() {
	//TODO: need to fix that
//	logicalDevice.destroy();

}

void initAll::initTexture(std::string const &path) {
	planetsTexture.init(path);
}
