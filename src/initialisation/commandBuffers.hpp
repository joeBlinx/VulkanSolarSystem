//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_COMMANDBUFFERS_HPP
#define SOLARSYSTEM_COMMANDBUFFERS_HPP


#include <vertex/vertex.hpp>
#include "swapChain.hpp"
#include "commandPool.hpp"
#include "renderPasses.hpp"
#include "frameBuffers.hpp"
#include "graphicsPipeline.hpp"
#include "descriptorSet.hpp"

class CommandBuffers {

	SwapChain & swapChain;
	CommandPool & commandPool;
	LogicalDevice & logicalDevice;
	RenderPasses & renderPasses;
	FrameBuffers &frameBuffers;
	GraphicsPipeline &graphicsPipeline;
	std::vector<VkCommandBuffer > commandBuffers;
	std::vector<VkCommandBuffer > secondaryCommandBuffers;
	Vertex & vertices;
	DescriptorSet &descriptorSet;
	UniformBuffer &uniform;

	void record(GraphicsPipeline &pipeline);
	void recordSecondary();
public:
	CommandBuffers(SwapChain &swapChain, CommandPool &commandPool, LogicalDevice &logicalDevice,
		               RenderPasses &renderPasses, FrameBuffers &frameBuffers,
		               GraphicsPipeline &graphicsPipeline, Vertex &vertices, DescriptorSet &descriptorSet,
		               UniformBuffer &uniform);

	VkCommandBuffer   operator[](int i)const{
		return commandBuffers[i];
	}
	VkCommandBuffer  operator[](int i){
		return commandBuffers[i];
	}

};


#endif //SOLARSYSTEM_COMMANDBUFFERS_HPP
