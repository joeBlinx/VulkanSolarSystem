//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_WINDOW_HPP
#define SOLARSYSTEM_WINDOW_HPP

#include <GLFW/glfw3.h>
#include "instance.hpp"

class Window{
	constexpr static int width = 1366;
	constexpr static int heigth = 768;

	GLFWwindow * window = nullptr;
	Instance instance;
public:

	Window();


	operator GLFWwindow*(){ return window;}

	static int getWidth();

	static int getHeigth();

	~Window();
};
#endif //SOLARSYSTEM_WINDOW_HPP
