//
// Created by stiven on 17-12-01.
//

#ifndef SOLARSYSTEM_DESCRIPTORSET_HPP
#define SOLARSYSTEM_DESCRIPTORSET_HPP


#include <uniformBuffer/uniformBuffer.hpp>
#include <texture/PlanetsTexture.hpp>
#include <texture/skyBox.hpp>
#include <uniformBuffer/uniform.hpp>
#include "logicaldevice.hpp"
#include "descriptorPool.hpp"
#include "descriptorSetLayout.hpp"

class DescriptorSet {
public:
	DescriptorSet(LogicalDevice &logicalDevice, DescriptorPool &descriptorPool,
		              DescriptorSetLayout &descriptorSetLayout, UniformBuffer &uni, PlanetsTexture &textures,
		              SkyBox &skybox, Uniform <object> &ob);

private:

	LogicalDevice & logicalDevice;
	VkDescriptorSet  descriptorSet;
	DescriptorPool & descriptorPool;
	DescriptorSetLayout & descriptorSetLayout;
	UniformBuffer & uniformBuffer;

	VkDescriptorSet skyBoxDescriptor;

	void createSkyBoxDescriptor(SkyBox &skyboox);
public:

	operator VkDescriptorSet (){ return descriptorSet;}

	VkDescriptorSet getSkyBoxDescriptor() const;
};


#endif //SOLARSYSTEM_DESCRIPTORSET_HPP
