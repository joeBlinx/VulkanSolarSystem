//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_GRAPHICSPIPELINE_HPP
#define SOLARSYSTEM_GRAPHICSPIPELINE_HPP


#include "logicaldevice.hpp"
#include "swapChain.hpp"
#include "renderPasses.hpp"
#include "descriptorSetLayout.hpp"
#include <vulkan/vulkan.h>
class GraphicsPipeline {

	LogicalDevice & logicalDevice;
	SwapChain& swapChain;
	RenderPasses & renderPasses;
	VkPipelineLayout pipelineLayout;
	VkPipeline  graphicsPipeline;
	VkPipeline stencilPipeline;


	VkPipelineLayout skyBoxLayout;
	VkPipeline skyBoxPipeline;


	void createSkyBox(DescriptorSetLayout &descriptorSetLayout);
public:
	GraphicsPipeline(LogicalDevice &logicalDevice, SwapChain &swapChain, RenderPasses &renderPasses,
		                 DescriptorSetLayout &descriptorSetLayout);

	VkShaderModule createShaderModule(const std::vector<char> &code);

	operator VkPipeline (){ return graphicsPipeline;}

	VkPipeline getSkyBoxPipeline() { return skyBoxPipeline;}

	virtual ~GraphicsPipeline();

	VkPipelineLayout getPipelineLayout() ;

	VkPipeline getStencilPipeline(){ return stencilPipeline;}

	VkPipelineLayout getSkyBoxLayout() const;
};


#endif //SOLARSYSTEM_GRAPHICSPIPELINE_HPP
