//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_COMMANDPOOL_HPP
#define SOLARSYSTEM_COMMANDPOOL_HPP


#include <vulkan/vulkan.h>
#include "logicaldevice.hpp"

class CommandPool {


	VkCommandPool commandPool;
	PhysicalDevice &physicalDevice;
	LogicalDevice &logicalDevice;
public:

	CommandPool(PhysicalDevice &physicalDevice, LogicalDevice &logicalDevice);

	operator VkCommandPool (){ return commandPool;}
	~CommandPool();

};


#endif //SOLARSYSTEM_COMMANDPOOL_HPP
