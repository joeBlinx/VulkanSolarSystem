//
// Created by stiven on 17-12-01.
//

#ifndef SOLARSYSTEM_DESCRIPTORSETLAYOUT_HPP
#define SOLARSYSTEM_DESCRIPTORSETLAYOUT_HPP


#include <vulkan/vulkan.h>
#include "logicaldevice.hpp"

class DescriptorSetLayout {


	VkDescriptorSetLayout descriptorSetLayout;

	VkDescriptorSetLayout descriptorSetLayoutCam;

	LogicalDevice & logicalDevice;

	void createLayoutCam();
public:

	DescriptorSetLayout(LogicalDevice &logicalDevice);
	operator VkDescriptorSetLayout (){ return descriptorSetLayout;}
	VkDescriptorSetLayout * getDesc(){ return &descriptorSetLayout;}
	VkDescriptorSetLayout * geDescCam(){ return &descriptorSetLayoutCam;}
	virtual ~DescriptorSetLayout();

};


#endif //SOLARSYSTEM_DESCRIPTORSETLAYOUT_HPP
