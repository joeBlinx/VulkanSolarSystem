//
// Created by stiven on 17-12-01.
//

#ifndef SOLARSYSTEM_DESCRIPTORPOOL_HPP
#define SOLARSYSTEM_DESCRIPTORPOOL_HPP


#include <vulkan/vulkan.h>
#include "logicaldevice.hpp"

class DescriptorPool {


	VkDescriptorPool descriptorPool;
	LogicalDevice & logicalDevice;

public:
	DescriptorPool(LogicalDevice &logicalDevice);
	operator VkDescriptorPool (){ return descriptorPool;}
	virtual ~DescriptorPool();

};


#endif //SOLARSYSTEM_DESCRIPTORPOOL_HPP
