//
// Created by stiven on 17-11-21.
//

#ifndef SOLARSYSTEM_DEPTHIMAGE_HPP
#define SOLARSYSTEM_DEPTHIMAGE_HPP


#include <vulkan/vulkan.h>
#include <vector>
#include "physicalDevice.hpp"
#include "logicaldevice.hpp"
#include "commandPool.hpp"
#include "swapChain.hpp"

class DepthImage {

	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthimageView;
	LogicalDevice & logicalDevice;
	PhysicalDevice &physicalDevice;
	CommandPool & commandPool;
	SwapChain & swapChain;

public:

	DepthImage(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, CommandPool &commandPool,
	           SwapChain &swapChain);


	static VkFormat
	findSupportedFormat(std::vector<VkFormat> const &candidatees, VkImageTiling tiling,
		                    VkFormatFeatureFlags features, PhysicalDevice &physicalDevice);
	static VkFormat findDepthFormat(PhysicalDevice &physicalDevice);


	operator VkImageView (){ return depthimageView;}
	~DepthImage();
};


#endif //SOLARSYSTEM_DEPTHIMAGE_HPP
