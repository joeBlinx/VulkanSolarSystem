//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_SWAPCHAIN_HPP
#define SOLARSYSTEM_SWAPCHAIN_HPP


#include <vulkan/vulkan.h>
#include <vector>
#include "instance.hpp"
#include "physicalDevice.hpp"
#include "logicaldevice.hpp"
struct swapChainSupportDetail{
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

class SwapChain {
	VkSwapchainKHR swapChain;
	LogicalDevice &  logicalDevice;
	PhysicalDevice & physicalDevice;

	std::vector<VkImage > swapChainImage;
	std::vector<VkImageView> swapChainImageViews;
	VkFormat swapChainFormat;
	VkExtent2D swapChainExtent;


private:
	swapChainSupportDetail detail;

	VkSurfaceFormatKHR chooseSwapChainSurfaceFormat();
	VkPresentModeKHR chooseSwapPresentMode();
	VkExtent2D chooseSwapExtent();
	void createImageView();
public:

	SwapChain(LogicalDevice &logicalDevice, PhysicalDevice &physicalDevice, Surface &surface);

	operator VkSwapchainKHR (){ return swapChain;}

	VkFormat getSwapChainFormat() const;

	const VkExtent2D &getSwapChainExtent() const;

	const std::vector<VkImageView> &getSwapChainImageViews() const;

	~SwapChain();
};


#endif //SOLARSYSTEM_SWAPCHAIN_HPP
