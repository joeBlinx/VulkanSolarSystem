//
// Created by stiven on 17-11-20.
//

#ifndef SOLARSYSTEM_INITALL_HPP
#define SOLARSYSTEM_INITALL_HPP


#include <uniformBuffer/uniformBuffer.hpp>
#include <scene.hpp>
#include <texture/PlanetsTexture.hpp>
#include <uniformBuffer/chooseObject.hpp>
#include <uniformBuffer/uniform.hpp>
#include "instance.hpp"
#include "surface.hpp"
#include "physicalDevice.hpp"
#include "logicaldevice.hpp"
#include "swapChain.hpp"
#include "graphicsPipeline.hpp"
#include "renderPasses.hpp"
#include "commandPool.hpp"
#include "depthImage.hpp"
#include "frameBuffers.hpp"
#include "commandBuffers.hpp"
#include "descriptorSetLayout.hpp"
#include "descriptorPool.hpp"
#include "descriptorSet.hpp"

class initAll {
	Window window;

private:

	Instance instance;
	Surface surface;
	PhysicalDevice physicalDevice;
	LogicalDevice logicalDevice;
	SwapChain swapChain;
	RenderPasses renderPasses;
	DescriptorSetLayout descriptorSetLayout;
	GraphicsPipeline graphicsPipeline;
	CommandPool commandPool;
	DepthImage depthImage;
	FrameBuffers frameBuffers;
	Vertex vertex;
	UniformBuffer uniformBuffer;
	PlanetsTexture planetsTexture;
	SkyBox skyBox;
	DescriptorPool descriptorPool;
	DescriptorSet  descriptorSet;
	CommandBuffers commandBuffers;

public:
	initAll();
	Window & getWindow() ;

	LogicalDevice & getLogicalDevice() ;

	SwapChain & getSwapChain() ;

	const CommandBuffers &getCommandBuffers() const;

	const GraphicsPipeline &getGraphicsPipeline() const;
	void update(const uniformArray &t, posArray const &pos);
	void initTexture(std::string const & path);

	virtual ~initAll();

};


#endif //SOLARSYSTEM_INITALL_HPP
