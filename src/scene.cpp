//
// Created by stiven on 17-11-20.
//

#include "scene.hpp"

uniformArray const Scene::getTranform() const {

	return model;
}

void Scene::move(glm::vec3 const &movement) {
	cam.translation(movement);
}

void Scene::rotate(glm::vec3 const &angle) {
	cam.rotation(angle);
}

Scene::Scene():sun("sun", "test", 1, {0, 0, 0}){
	planets.reserve(Scene::numberPlanet);
	planets.emplace_back("earth", "test2", 5, glm::vec3{50, 0, 0}, 1, 365, sun);
	planets.emplace_back("mars", "test2", 20, glm::vec3{100, 0, 0}, 1, 686, sun);
	planets.emplace_back("jupiter", "test2", 5, glm::vec3{150, 0, 0}, 1, 200, sun);
	planets.emplace_back("pouet", "test2", 10, glm::vec3{200, 0, 0}, 1, 120, sun);

}

void Scene::update(float delta) {
	model[0].model = sun.update(delta);
	positions[0].pos1 = sun.getPos();
	positions[0].size = sun.getSize();
	for(unsigned i = 0; i < Scene::numberPlanet-1 ; i++){
		model[i+1].model = planets[i].update(delta);
		positions[i+1].pos1 = planets[i].getUpdatePosition();
		positions[i+1].size= planets[i].getSize();
	}
	for(auto & a: model) {
		a.view = cam.getView();
		a.proj = cam.getProj();
	}
}

posArray const &Scene::getPos() {
	return positions;
}

void Scene::rotateKeepLooking(const glm::vec3 &angle) {
	cam.rotateAndKeepLooking(angle);

}

