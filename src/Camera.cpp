//
// Created by lysilia on 18/11/17.
//

#include "Camera.h"

Camera::Camera(): pos(0, 0, 500.0), dir(0.0, 0.0, -1.0), up(0.0, 1.0, 0.0), x(glm::cross(dir, up)),
                  proj(glm::perspective(glm::radians(45.0f), 1366.0f/768.0f, 0.1f, 1000.f)){
	updateView();
}

glm::mat4 Camera::cameraMat() {
    return proj * view;
}

glm::mat4 Camera::getView()const {
    return view;
}

glm::mat4 Camera::getProj() const {
    return proj;
}

void Camera::translation(glm::vec3 delta) {
    pos += delta.x * x;
    pos += delta.y * up;
    pos += delta.z * dir;
    updateView();
}

void Camera::rotation(glm::vec3 angles) {

    glm::mat4 rotX = glm::rotate(glm::mat4(), angles.x, x);
    glm::mat4 rotZ = glm::rotate(glm::mat4(), angles.z, dir);
    glm::mat4 rotY = glm::rotate(glm::mat4(), angles.y, up);

    glm::mat3 totRot = glm::mat3(rotX * rotY * rotZ);

    x = totRot * x;
    up = totRot * up;
    dir = totRot * dir;

    updateView();
}

void Camera::updateView() {
    view = glm::lookAt(pos, pos+dir, up);
}

void Camera::rotateAndKeepLooking(glm::vec3 const &angles) {

    glm::mat4 rotX = glm::rotate(glm::mat4(), angles.x, x);
    glm::mat4 rotZ = glm::rotate(glm::mat4(), angles.z, dir);
    glm::mat4 rotY = glm::rotate(glm::mat4(), angles.y, up);

    glm::mat3 totRot = glm::mat3(rotX * rotY * rotZ);

	pos = totRot*pos;
	x = totRot * x;
    up = totRot * up;
    dir = totRot * dir;

//	dir = lookingPoint-pos;
	updateView();
}
