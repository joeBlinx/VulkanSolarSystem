//
// Created by lysilia on 18/11/17.
//

#ifndef VALKYRIA_CAMERA_H
#define VALKYRIA_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

class Camera {

private:

    glm::vec3 pos;
    glm::vec3 dir;
    glm::vec3 up;
    glm::vec3 x;

    glm::mat4 view;
    glm::mat4 proj;

    void updateView();

public:

    Camera();

    glm::mat4 cameraMat();
    void translation(glm::vec3 delta);
    void rotation(glm::vec3 angles);
	void rotateAndKeepLooking(glm ::vec3 const & angle);
    glm::mat4 getView() const;
    glm::mat4 getProj() const;

};


#endif //VALKYRIA_CAMERA_H
