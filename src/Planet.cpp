//
// Created by lysilia on 18/11/17.
//

#include <iostream>
#include "Planet.h"
#include "cmath"
#ifdef WIN32
#define M_PI 3.14159
#endif
Planet::Planet(const std::string &name, const std::string &model, float size, const glm::vec3 &pos, float dayTime, float yearTime,
               const Sun &origine) : Sun(name, model, size, pos), dayTime(dayTime),
                                                                 yearTime(yearTime), origine(origine){

    // solar velocity  = angle : temps que la planet parcourt en 1 jour
    orbitalVelocity = (float)(2* M_PI) / yearTime;
    ownVelocity = (float)(2* M_PI) / dayTime;

    tutu = false;

}

glm::mat4 Planet::update(float deltaTime){

    // matrice de rotation sur elle-meme
    glm::mat4 rotYown = glm::rotate(glm::mat4(), ownVelocity * deltaTime, glm::vec3(0.0, 1.0, 0.0));

    // matrice de translation
    glm::mat4 transToOrigine = glm::translate(glm::mat4(), pos);

    // matrice de translation
    glm::mat4 transToOrigine1 = glm::translate(glm::mat4(), -pos);

    // matrice de rotation autour de l'origine
    glm::mat4 rotYsolar = transToOrigine1 * glm::rotate(glm::mat4(), orbitalVelocity * deltaTime, glm::vec3(0.0, 1.0, 0.0)) * transToOrigine;
   // glm::mat4 rotYsolar = glm::rotate(transToOrigine1, orbitalVelocity * deltaTime, glm::vec3(0.0, 1.0, 0.0)) * transToOrigine;
   // glm::mat4 rotYsolar = glm::rotate(glm::mat4(), orbitalVelocity * deltaTime, glm::vec3(origine.getUpdatePosition().x, 1.0, origine.getUpdatePosition().z));

    // matrice de translation
    glm::mat4 trans = glm::translate(glm::mat4(), origine.getUpdatePosition() + pos);

    // matrice de scale
    glm::mat4 scale = glm::scale(glm::vec3(size));

    // attention a l'ordre : inverse car sommets a droite
    glm::mat4 planetModel = trans * rotYsolar * rotYown * scale;

    //update position
    updatePosition = glm::vec3(trans * rotYsolar * glm::vec4(0.0, 0.0, 0.0, 1.0));


    return planetModel;
}


bool Planet::operator<(const Planet& bob){
	return name < bob.name;
}