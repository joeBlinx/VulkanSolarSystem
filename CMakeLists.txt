cmake_minimum_required(VERSION 3.8)
project(SolarSystem)

set(CMAKE_CXX_STANDARD 17)

include_directories(src/)
if (WIN32)
	find_package(VULKAN REQUIRED)
	if(${VULKAN_FOUND})
		message("vulkan found")
	else()
		message("vulkan not found")
	endif()
	include_directories(${VULKAN_INCLUDE_DIRS})
	include_directories(BEFORE_SYSTEM ./)
	include_directories(BEFORE_SYSTEM PUBLIC glfw/include)
	include_directories(C:\\VulkanSDK\\1.0.61.1\\Include)
	link_directories(C:\\VulkanSDK\\1.0.61.1\\Lib32)
	link_directories(glfw/lib)
		include_directories($ENV{LIB_SDK}/include)
	link_directories(lib)
	add_compile_options("/std:c++latest")

else(WIN32)
	add_compile_options(-pg -Wall -Wextra -pedantic )
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
	SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
	SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
endif(WIN32)



file(GLOB_RECURSE HPP_FILES src/*.hpp)
file(GLOB_RECURSE CPP_FILES src/*.cpp)


set(SOURCES ${HPP_FILES} ${CPP_FILES})

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})

add_executable(SolarSystem ${SOURCES})

set_target_properties( ${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} )
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR})
if (WIN32)
	target_link_libraries(${PROJECT_NAME} glfw3 vulkan-1 glEngine utils)
else(WIN32)
	target_link_libraries(${PROJECT_NAME} glfw3 vulkan  Xcursor -lX11 -lXxf86vm -lXrandr -lpthread -lXi dl -lXi -lXinerama glEngine utils)
endif(WIN32)

add_definitions(-DGLM_FORCE_RADIANS -DGLM_FORCE_DEPTH_ZERO_TO_ONE -DGLFW_INCLUDE_VULKAN)